const express = require('express')
const app = express()
const port = 3000

var player = {};
var team_t = {};
var team_ct = {};

var connected = false;

app.use(express.json());
app.use(express.static('html'));

app.get('/', (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    player.steamid = "HIDDEN";
    const response = {player, team_t, team_ct};
    res.send(JSON.stringify(response));
})

app.post("/", (req, res, next) => {
    player = req.body.player;
    if (req.body.map) {
        team_t = req.body.map.team_t;
        team_ct = req.body.map.team_ct;
    }

    if(!connected){
        console.log("CSGO connected!");
        connected = true;
    }

    next();
});

app.listen(port, () => {
    console.log(`Britsch listening on port ${port}`);
})
